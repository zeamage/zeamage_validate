================
zeamage validate
================


.. image:: https://img.shields.io/pypi/v/zeamage_validate.svg
        :target: https://pypi.python.org/pypi/zeamage_validate

.. image:: https://img.shields.io/travis/farmlab/zeamage_validate.svg
        :target: https://travis-ci.org/farmlab/zeamage_validate

.. image:: https://readthedocs.org/projects/zeamage-validate/badge/?version=latest
        :target: https://zeamage-validate.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Optimization and evalutation toolkit for zeamage


* Free software: MIT license
* Documentation: https://zeamage-validate.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
