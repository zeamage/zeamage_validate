# -*- coding: utf-8 -*-

"""Top-level package for zeamage validate."""

__author__ = """Jérôme Dury"""
__email__ = 'jerome.dury@flyingsheep.fr'
__version__ = '0.1.0'
